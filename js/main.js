function searchExpand ($form) {
	var left = -($form.closest('.search').offset().left - $form.closest('.wrapper').offset().left);

	if (!$form.hasClass('search-form-wide')) {
		$form.addClass('search-form-wide').css({
			left: left
		}).find('.search-form-content').width(-left + 40);
		$form.find('.search-form-input').focus();
	}
}

function showPopup (popupId) {
	var
		$popup = $('#' + popupId),
		$overlay = $('.js-overlay');

	$('.popup').hide();
	$overlay.css({display: 'flex'});
	$popup.show();

	return false;
}

function hidePopup () {
	$('.js-overlay').hide();
}

$(document).ready(function() {
	$('.js-search-switcher').on('click', function() {
		var $form = $(this).parent();

		if (!$form.hasClass('search-form-wide')) {
			searchExpand($form);
		} else {
			$form.removeClass('search-form-wide').css({left: 0});
		}
	});

	$('.search-form-button').on('submit', function() {
		var $form = $(this).closest('.search-form');

		$form.addClass('search-form-blocked');
		searchExpand($form);
	});

	$('.custom-select').customSelectMenu();

	$('.js-main-decor').owlCarousel({
		items: 1,
		dots: true,
		autoplay: true,
		autoplayHoverPause: true,
		loop: true
	});

	$('.js-partners').owlCarousel({
		items: 1,
		dots: true,
		autoplay: true,
		autoplayHoverPause: true,
		loop: true
	});

	$('.js-gallery').each(function() {
		var $this = $(this);

		if ($this.find('.gallery-item').length > 4) {
			$this.addClass('owl-carousel').owlCarousel({
				items: 4,
				nav: true
			});
		}
	});

	$('.js-popup-button').on('click', function () {
		showPopup($(this).data('popup'));
		return false;
	});

	$('.js-popup-close').on('click', function () {
		hidePopup();
	});

	$('.js-mask-phone').mask('+7 (999) 99-99-999');

	$().fancybox({
		selector: '[data-fancybox="gallery"]',
		buttons: ['close']
	});
});